from typing import Dict, List, Optional, Tuple
import poetry_version

__version__ = poetry_version.extract(source_file=__file__)

def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List[Dict], bool]:

    blank_symbol = machine["blank"]
    initial_state = machine["start state"]
    final_states = machine["final states"]
    transition_table = machine["table"]

    tape: List[str] = list(input_)
    head_position = 0
    current_state = initial_state
    execution_history = []
    step = 0

    while current_state not in final_states:
        if steps is not None and step >= steps:
            break

        if head_position < 0:
            tape.insert(0, blank_symbol)
            head_position = 0
        elif head_position >= len(tape):
            tape.append(blank_symbol)

        symbol = tape[head_position]

        if symbol not in transition_table.get(current_state, {}):
            break

        transition = transition_table[current_state][symbol]

        history_entry = {
            'state': current_state,
            'reading': symbol,
            'position': head_position,
            'memory': ''.join(tape),
            'transition': transition
        }
        execution_history.append(history_entry)

        if isinstance(transition, str):
            if transition == "L":
                head_position -= 1
            elif transition == "R":
                head_position += 1
        else:
            if "write" in transition:
                tape[head_position] = transition['write']
            if "L" in transition:
                head_position -= 1
                current_state = transition['L']
            elif "R" in transition:
                head_position += 1
                current_state = transition['R']
            else:
                break

        step += 1

    result = "".join(tape).strip(blank_symbol)
    return result, execution_history, current_state in final_states
